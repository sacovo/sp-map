const element = document.querySelector("#map");
const info = document.querySelector("#info");

const createSectionElement = (section) => {
  let element = document.createElement("div");
  element.classList.add("section");
  element.innerHTML = `
        <h2 id="name">${section.name || ""}</h2>
        <address>
          <span class="stree">${section.street || ""}</span>
          <br />
          <span class="city">${section.zip || ""} ${section.city || ""}</span>
        </address>
      `;
  if (section.tel) {
    element.innerHTML += `<a class="tel" href="tel:${section.tel}">${section.tel}</a>`;
  }
  if (section.website) {
    element.innerHTML += `<a class="website" href="${section.website}">Website</a>`;
  }
  if (section.email) {
    element.innerHTML += `<a class="email" href="mailto:${section.email}">E-Mail</a>`;
  }
  return element;
};

const showSection = (resp) => {
  info.innerHTML = "";
  if (!(typeof resp[Symbol.iterator] === "function")) {
    info.classList.add("empty");
    return;
  }
  info.classList.remove("empty");

  for (const i of resp) {
    info.appendChild(createSectionElement(i.section));
  }
};

// Fetch the json
const topoJson = await fetch(element.dataset.json).then((response) =>
  response.json()
);

let lakeName = "K4seen_yyymmdd11";
let communeName = "K4voge_20200101_gf";
let cantonName = "K4kant_19970101_gf";
let switzerlandName = "K4suis_18480101_gf";

Object.keys(topoJson.objects).forEach((name) => {
  if (name.includes("seen")) {
    lakeName = name;
  } else if (name.includes("voge")) {
    communeName = name;
  } else if (name.includes("kant")) {
    cantonName = name;
  } else if (name.includes("suis")) {
    switzerlandName = name;
  }
});

function getHeight(width, topoJson) {
  const [minX, minY, maxX, maxY] = topojson.bbox(topoJson);
  return ((maxY - minY) / (maxX - minX)) * width;
}

function getProjection(width, topoJson) {
  const [minX, minY, maxX, maxY] = topojson.bbox(topoJson);
  const height = ((maxY - minY) / (maxX - minX)) * width;

  const x = d3.scaleLinear().range([0, width]).domain([minX, maxX]);

  const y = d3.scaleLinear().range([0, height]).domain([maxY, minY]);

  return d3.geoTransform({
    point: function (px, py) {
      this.stream.point(x(px), y(py));
    },
  });
}

const map = d3.select("#map");

const zoom = d3
  .zoom()
  .scaleExtent([0.8, 12])
  .on("zoom", (event) => zoomed(event));

const maxHeight = element.offsetHeight;

let width =
  window.innerWidth < 900 ? window.innerWidth : window.innerWidth - 200;
let height = getHeight(width, topoJson);

if (height > maxHeight) {
  width = width * (maxHeight / height);
  height = maxHeight;
}

console.log(width);

map.html("");

const svg = map
  .append("svg")
  .attr("viewbox", `0 0 ${window.innerWidth} ${height}`)
  .attr("width", "100%")
  .attr("height", "100%")
  .attr("preserveAspectRatio", "xMinYMin meet")
  .attr("version", "1.1");

const g = svg.append("g").attr("id", "mapRoot");
//  .attr("transform", `translate(${window.innerWidth / 2 - width / 2},0)`);

const zoomed = (event) => {
  const { transform } = event;
  g.attr("transform", transform);
};

svg.call(zoom).on("dblclick.zoom", null);

const path = d3.geoPath(getProjection(width, topoJson));

const appendFeatures = (geo, cssClass, clicked) => {
  let innerG = g.append("g");

  innerG
    .selectAll("path")
    .data(geo.features)
    .enter()
    .append("path")
    .attr("cursor", clicked ? "pointer" : "default")
    .attr("id", function (d) {
      if (d.properties.vogeId) {
        return `${cssClass}_${d.properties.vogeId}`;
      }
      return `${cssClass}_${d.properties.id}`;
    })
    .attr("class", cssClass)
    .on("click", clicked)
    .attr("d", path);

  return innerG;
};

const clearSelected = (className) => {
  const elements = document.getElementsByClassName(className);

  for (const element of elements) {
    element.classList.remove("selected");
  }
};

const idToShort = [
  "ZH",
  "BE",
  "LU",
  "UR",
  "SZ",
  "OW",
  "NW",
  "GL",
  "ZG",
  "FR",
  "SO",
  "BS",
  "BL",
  "SH",
  "AR",
  "AI",
  "SG",
  "GB",
  "AG",
  "TG",
  "TI",
  "VD",
  "VS",
  "NE",
  "GE",
  "JU",
];

const fetchCantonInfo = async (id) => {
  const short = idToShort[id - 1];
  const url = `https://tel.sp-ps.ch/sections/by_gde_nr_or_kanton/${short}`;
  const info = await fetch(url).then((response) => response.json());
  return info;
};

const fetchCommuneInfo = async (id) => {
  const url = `https://tel.sp-ps.ch/sections/by_gde_nr_or_kanton/${id}`;
  const info = await fetch(url).then((response) => response.json());
  return info;
};

appendFeatures(
  topojson.feature(topoJson, topoJson.objects[communeName]),
  "commune",
  async (event, obj) => {
    event.stopPropagation();
    if (event.detail === 1) {
      zoomToBounds(obj);
      clearSelected("commune");
      event.target.classList.add("selected");
      showSection(await fetchCommuneInfo(obj.properties.id));
    } else if (event.detail === 2) {
      reset();
    }
  }
);

g.append("path")
  .datum(
    topojson.mesh(topoJson, topoJson.objects[communeName], (a, b) => a !== b)
  )
  .attr("fill", "none")
  .attr("d", path);

appendFeatures(
  topojson.feature(topoJson, topoJson.objects[cantonName]),
  "canton",
  async (event, obj) => {
    event.stopPropagation();
    clearSelected("canton");
    clearSelected("commune");
    event.target.classList.add("selected");
    zoomToBounds(obj);
    showSection(await fetchCantonInfo(obj.properties.id));
  }
);

appendFeatures(topojson.feature(topoJson, topoJson.objects[lakeName]), "lake");

const switzerland = topojson.feature(
  topoJson,
  topoJson.objects[switzerlandName]
);
appendFeatures(switzerland, "switzerland");
const reset = () => {
  clearSelected("canton");
  clearSelected("commune");
  zoomToBounds(switzerland);

  info.innerHTML = "";
};

const zoomToBounds = (obj) => {
  let [[x0, y0], [x1, y1]] = path.bounds(obj);

  svg
    .transition()
    .duration(400)
    .call(
      zoom.transform,
      d3.zoomIdentity
        .translate(width, height / 2)
        .scale(
          Math.min(10, 0.9 / Math.max((x1 - x0) / width, (y1 - y0) / height))
        )
        .translate(-(x0 + x1) / 2, -(y0 + y1) / 2)
    );
};

svg.call(
  zoom.transform,
  d3.zoomIdentity.translate(window.innerWidth / 2 - width / 2, 0)
);
